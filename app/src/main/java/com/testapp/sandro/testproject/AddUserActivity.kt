package com.testapp.sandro.testproject

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_user.*


class AddUserActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_ID = "com.testapp.sandro.testproject.EXTRA_ID"
        const val EXTRA_FULLNAME = "com.testapp.sandro.testproject.EXTRA_FULLNAME"
        const val EXTRA_EMAIL = "com.testapp.sandro.testproject.EXTRA_EMAIL"
        const val EXTRA_BIRTHDAY = "com.testapp.sandro.testproject.EXTRA_BIRTHDAY"
        const val EXTRA_BIO = "com.testapp.sandro.testproject.EXTRA_BIO"
        const val EXTRA_IMAGE = "com.testapp.sandro.testproject.IMAGE"
        //image pick code
        private const val IMAGE_PICK_CODE = 1000
        //Permission code
        private const val PERMISSION_CODE = 1001
    }

    private var imageURI: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)


        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_launcher_background)

        title = "Add user"

        img_pick_btn.setOnClickListener {
            //check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED
                ) {
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE)
                } else {
                    //permission already granted
                    pickImageFromGallery()
                }
            } else {
                //system OS is < Marshmallow
                pickImageFromGallery()
            }
        }


    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_user_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.save_user -> {
                saveUser()
                true
            }

            else -> super.onOptionsItemSelected(item!!)
        }
    }

    private fun saveUser() {
        if (edit_text_fullName.text.toString().trim().isBlank() || edit_text_email.text.toString().trim().isBlank()) {
            Toast.makeText(this, "Fields can't be empty!", Toast.LENGTH_SHORT).show()
            return
        }

        val data = Intent().apply {
            putExtra(EXTRA_FULLNAME, edit_text_fullName.text.toString())
            putExtra(EXTRA_EMAIL, edit_text_email.text.toString())
            putExtra(EXTRA_BIRTHDAY, edit_text_birthday.text.toString())
            putExtra(EXTRA_BIO, edit_text_bio.text.toString())
            putExtra(EXTRA_IMAGE, imageURI)

            if (intent.getIntExtra(EXTRA_ID, -1) != -1) {
                putExtra(EXTRA_ID, intent.getIntExtra(EXTRA_ID, -1))
            }
        }

        setResult(Activity.RESULT_OK, data)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            val imageData = data?.data!!
            imageURI = imageData.toString()
            if (imageURI.isEmpty()) {
                image_view.setImageResource(R.drawable.ic_launcher_foreground)
            }
            else {
                Picasso.get()
                    .load(imageData)
                    .fit()
                    .centerCrop()
                    .into(image_view)
            }



        }
    }
}
