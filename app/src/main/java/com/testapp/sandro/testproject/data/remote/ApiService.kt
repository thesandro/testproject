package com.testapp.sandro.testproject.data.remote

import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @POST("demo")
    suspend fun getLatestNewsAsync(
//        @Query("listing_id") id: Int,
//        @Header("Authorization") token:String
    ): Response<String>
}