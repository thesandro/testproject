package com.testapp.sandro.testproject.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.testapp.sandro.testproject.R
import com.testapp.sandro.testproject.data.local.User
import kotlinx.android.synthetic.main.user_item.view.*

class UserAdapter : ListAdapter<User, UserAdapter.UserHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.fullName == newItem.fullName && oldItem.email == newItem.email
                        && oldItem.birthday == newItem.birthday
            }
        }
    }

    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false)
        return UserHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        val currentUser: User = getItem(position)

        holder.textViewFullName.text = currentUser.fullName
        holder.textViewEmail.text = currentUser.email
        holder.textViewBirthday.text = currentUser.birthday

        if (currentUser.image.isEmpty()) {
            holder.imageView.setImageResource(R.drawable.ic_launcher_foreground)
        }
        else {
            Picasso.get()
                .load(Uri.parse(currentUser.image))
                .fit()
                .centerCrop()
                .into(holder.imageView)
        }
    }

    fun getUserAt(position: Int): User {
        return getItem(position)
    }

    inner class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    listener?.onItemClick(getItem(position))
                }
            }
        }

        var textViewFullName: TextView = itemView.text_view_fullName
        var textViewEmail: TextView = itemView.text_view_email
        var textViewBirthday: TextView = itemView.text_view_birthday
        var imageView:ImageView = itemView.image_view_profile
    }

    interface OnItemClickListener {
        fun onItemClick(user: User)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }
}