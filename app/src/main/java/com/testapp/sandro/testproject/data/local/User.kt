package com.testapp.sandro.testproject.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ColumnInfo



@Entity(tableName = "user_table")
data class User(
    var fullName: String,

    var email: String,

    var birthday: String,

    var bio: String,

    var image:String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}