package com.testapp.sandro.testproject

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.testapp.sandro.testproject.adapters.UserAdapter
import com.testapp.sandro.testproject.data.local.User
import com.testapp.sandro.testproject.data.remote.ApiService
import com.testapp.sandro.testproject.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    companion object {
        const val ADD_USER_REQUEST = 1
    }

    private lateinit var userViewModel: UserViewModel
    private lateinit var apiService: ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonAddUser.setOnClickListener {
            startActivityForResult(
                Intent(this, AddUserActivity::class.java),
                ADD_USER_REQUEST
            )
        }


        apiService = retrofit().create(ApiService::class.java)
        GlobalScope.launch(Dispatchers.Main){

            val primaryNewsResponse = callLatestNewsAsync()
            d("resultis", primaryNewsResponse.body()!!)

        }
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)

        val adapter = UserAdapter()

        recycler_view.adapter = adapter

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        userViewModel.getAllUsers().observe(this, Observer<List<User>> {
            for(item in it){
            d("getUser:", item.fullName)
            }
            adapter.submitList(it)
        })
        adapter.setOnItemClickListener(object : UserAdapter.OnItemClickListener {
            override fun onItemClick(user: User) {
                val intent = Intent(baseContext, DetailsActivity::class.java)
                intent.putExtra(AddUserActivity.EXTRA_ID, user.id)
                intent.putExtra(AddUserActivity.EXTRA_FULLNAME, user.fullName)
                intent.putExtra(AddUserActivity.EXTRA_EMAIL, user.email)
                intent.putExtra(AddUserActivity.EXTRA_BIRTHDAY, user.birthday)
                intent.putExtra(AddUserActivity.EXTRA_BIO, user.bio)
                intent.putExtra(AddUserActivity.EXTRA_IMAGE, user.image)
                startActivity(intent)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.delete_all_users -> {
                userViewModel.viewModelScope.launch(IO){
                    userViewModel.deleteAllUsers()
                }
                Toast.makeText(this, "All users deleted!", Toast.LENGTH_SHORT).show()


                true
            }
            else -> {
                super.onOptionsItemSelected(item!!)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ADD_USER_REQUEST && resultCode == Activity.RESULT_OK) {
            val newUser = User(
                data!!.getStringExtra(AddUserActivity.EXTRA_FULLNAME)!!,
                data.getStringExtra(AddUserActivity.EXTRA_EMAIL)!!,
                data.getStringExtra(AddUserActivity.EXTRA_BIRTHDAY)!!,
                data.getStringExtra(AddUserActivity.EXTRA_BIO)!!,
                data.getStringExtra(AddUserActivity.EXTRA_IMAGE)!!
            )
            userViewModel.viewModelScope.launch(IO){
                userViewModel.insert(newUser)
            }
            Toast.makeText(this, "User saved!", Toast.LENGTH_SHORT).show()



        } else {
            Toast.makeText(this, "User not saved!", Toast.LENGTH_SHORT).show()
        }


    }
    private suspend fun callLatestNewsAsync(): Response<String> = apiService.getLatestNewsAsync()


    fun retrofit() : Retrofit = Retrofit.Builder()
        .baseUrl("http://0.0.0.0:8080") // YOUR BASE URL OF API
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}
