package com.testapp.sandro.testproject.viewmodels

import android.app.Application
import android.util.Log.d

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.testapp.sandro.testproject.data.local.User
import com.testapp.sandro.testproject.data.UserRepository

class UserViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: UserRepository =
        UserRepository(application)
    private var allUsers: LiveData<List<User>> = repository.getAllUsers()

    suspend fun insert(user: User) {
        repository.insert(user)
    }

    suspend fun deleteAllUsers() {
        repository.deleteAllUsers()
    }

    fun getAllUsers(): LiveData<List<User>> {
        if(allUsers.value!=null)
        for(item in allUsers.value!!) {
            d("getuser viewmodel ", item.fullName)
        }
        return allUsers
    }
}