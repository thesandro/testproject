package com.testapp.sandro.testproject.data

import android.app.Application
import androidx.lifecycle.LiveData
import com.testapp.sandro.testproject.data.local.User
import com.testapp.sandro.testproject.data.local.UserDao
import com.testapp.sandro.testproject.data.local.UserDatabase

class UserRepository(application: Application){
    private var userDao: UserDao

    private var allUsers: LiveData<List<User>>

    init {
        val database: UserDatabase = UserDatabase.getInstance(
            application.applicationContext
        )
        userDao = database.userDao()
        allUsers = userDao.getAllUsers()
    }

    suspend fun insert(user: User) {
        userDao.insert(user)
    }

    suspend fun deleteAllUsers() {
        userDao.deleteAllUsers()
    }

    fun getAllUsers(): LiveData<List<User>> {
        return allUsers
    }

}