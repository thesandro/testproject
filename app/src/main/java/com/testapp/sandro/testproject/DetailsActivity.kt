package com.testapp.sandro.testproject


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_launcher_background)

        if (intent.hasExtra(AddUserActivity.EXTRA_ID)) {
            title = "User Details"
            text_view_details_fullName.text = intent.getStringExtra(AddUserActivity.EXTRA_FULLNAME)
            text_view_details_email.text = intent.getStringExtra(AddUserActivity.EXTRA_EMAIL)
            text_view_details_birthday.text = intent.getStringExtra(AddUserActivity.EXTRA_BIRTHDAY)
            text_view_details_bio.text = intent.getStringExtra(AddUserActivity.EXTRA_BIO)
            if (intent.getStringExtra(AddUserActivity.EXTRA_IMAGE)!!.isEmpty()) {
                image_view_details.setImageResource(R.drawable.ic_launcher_foreground)
            }
            else {
                Picasso.get()
                    .load(intent.getStringExtra(AddUserActivity.EXTRA_IMAGE))
                    .fit()
                    .centerCrop()
                    .into(image_view_details)
            }
        }
    }
}
